package application.filemanager;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.activation.MimetypesFileTypeMap;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.primefaces.event.DragDropEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import application.database.MongoConst;
import application.database.MongoFileAO;
import application.session.UserSession;

@ManagedBean
@ViewScoped
public class FileOverviewBean {
	@ManagedProperty(value="#{mongoFileAO}")
	private MongoFileAO fileAO;

	@ManagedProperty(value="#{userSession}")
	private UserSession userSession;
	
	private List<ShowFile> dropedFiles;
	private FileDataModel fileDataModel;
	private ShowFile selectedFile;
	
	public FileOverviewBean() {}
	
	@PostConstruct
	private void fileOverviewBeanConstructor() {
		fileDataModel = new FileDataModel(
				fileAO.getFileNamesFor(
						userSession.getUsername()));
		dropedFiles = new ArrayList<ShowFile>();
	}
	
	public void onDropFile(DragDropEvent ddEvent) {
		ShowFile showFile = (ShowFile)ddEvent.getData();
		dropedFiles.add(showFile);
	}
	
	public UserSession getUserSession() {
		return userSession;
	}

	public void setUserSession(UserSession userSession) {
		this.userSession = userSession;
	}

	public FileDataModel getFileDataModel() {
		return fileDataModel;
	}

	public ShowFile getSelectedFile() {
		return selectedFile;
	}

	public void setSelectedFile(ShowFile selectedFile) {
		this.selectedFile = selectedFile;
	}

	public MongoFileAO getFileAO() {
		return fileAO;
	}
	
	public void setFileAO(MongoFileAO fileAO) {
		this.fileAO = fileAO;
	}
	
	public List<ShowFile> getDropedFiles() {
		return dropedFiles;
	}
	
	public StreamedContent getZipDownloadFile() {
		if(dropedFiles.size() == 0) {
			//add error handling
			return null;
		}
		
		try {
			InputStream in = null;
			File file = new File(MongoConst.TEMP_PATH + userSession.getUsername() + ".zip");
			file.createNewFile();
		
			ZipOutputStream zipOutputStream = new ZipOutputStream(new FileOutputStream(file));
			
			String name;
			int read;
			byte[] bytes = new byte[1024];
			for(ShowFile showFile : dropedFiles) {
				name = showFile.getFileName();
				in = fileAO.readFileByUser(userSession.getUsername(), name);
				zipOutputStream.putNextEntry(new ZipEntry(name));
				while ((read = in.read(bytes)) != -1) {
					zipOutputStream.write(bytes, 0, read);
				}
				in.close();
			}
			zipOutputStream.close();
		
			FileInputStream inputStream = new FileInputStream(file);
			file.delete();
			return new DefaultStreamedContent(
					inputStream, 
					new MimetypesFileTypeMap().getContentType(file), 
					userSession.getUsername() + ".zip");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public StreamedContent getDownloadFile() {
		try {
			String fileName = selectedFile.getFileName();
			InputStream inputStream = fileAO.readFileByUser(
					userSession.getUsername(), fileName);
			return new DefaultStreamedContent(
					inputStream, 
					new MimetypesFileTypeMap().getContentType(fileName), 
					fileName);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public void deleteFile() {
		fileAO.deleteFileInDB(
				userSession.getUsername(),
				selectedFile.getFileName());
		fileDataModel = new FileDataModel(
				fileAO.getFileNamesFor(
						userSession.getUsername()));
	}
}