package application.filemanager;

import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

public class FileDataModel extends ListDataModel<ShowFile> implements SelectableDataModel<ShowFile> {
	public FileDataModel() {}
	
	public FileDataModel(List<ShowFile> data) {
		super(data);
	}
	
	@Override
	public ShowFile getRowData(String rowKey) {
		@SuppressWarnings("unchecked")
		List<ShowFile> files = (List<ShowFile>)getWrappedData();
		
		for(ShowFile file : files) {
			if(file.getFileName().equals(rowKey)) {
				return file;
			}
		}
		
		return null;
	}

	@Override
	public Object getRowKey(ShowFile file) {
		return file.getFileName();
	}

}
