package application.filemanager;

public class ShowFile {
	private String fileName;
	
	public ShowFile(String name) {
		fileName = name;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}
