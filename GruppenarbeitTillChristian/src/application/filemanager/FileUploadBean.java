package application.filemanager;

import java.io.IOException;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import org.primefaces.event.FileUploadEvent;

import application.database.MongoFileAO;
import application.session.UserSession;

@ManagedBean
@SessionScoped
public class FileUploadBean {
	@ManagedProperty(value="#{mongoFileAO}")
	private MongoFileAO fileAO;

	@ManagedProperty(value="#{userSession}")
	private UserSession userSession;
	
	public void handleFileUpload(FileUploadEvent event) {
		try {
			fileAO.writeFileToDB(
					event.getFile().getInputstream(),
					event.getFile().getFileName(),
					userSession.getUsername());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public MongoFileAO getFileAO() {
		return fileAO;
	}

	public void setFileAO(MongoFileAO fileAO) {
		this.fileAO = fileAO;
	}

	public UserSession getUserSession() {
		return userSession;
	}

	public void setUserSession(UserSession userSession) {
		this.userSession = userSession;
	}
}
