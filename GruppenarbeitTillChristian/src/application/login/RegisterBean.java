package application.login;

import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

import application.database.MongoUserAO;

@ViewScoped
@ManagedBean
public class RegisterBean {
	@ManagedProperty(value = "#{mongoUserAO}")
	private MongoUserAO mongoUserAO;
	

	private String username;
	private String password;
	private String email;




	public String register() {
		mongoUserAO.addUser(username, password, email);
		return "login";
	}

	
	
	public void validateUsername(FacesContext context, UIComponent component,
			Object value) {
		
		boolean doesExist = mongoUserAO.isUserAlreadyExisting(value.toString());
				

		if (doesExist == true) {
			
			ResourceBundle bundle = ResourceBundle.getBundle("language.error", context.getViewRoot().getLocale());
		    
			
			FacesMessage message = new FacesMessage(bundle.getString("username_already_used"));
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(message);
		}
	}
	
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public MongoUserAO getMongoUserAO() {
		return mongoUserAO;
	}

	public void setMongoUserAO(MongoUserAO mongoUserAO) {
		this.mongoUserAO = mongoUserAO;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	
	

}
