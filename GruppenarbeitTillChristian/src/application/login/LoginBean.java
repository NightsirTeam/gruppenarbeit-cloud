package application.login;

import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

import application.database.MongoUserAO;
import application.datamodel.User;
import application.session.UserSession;

@ViewScoped
@ManagedBean
public class LoginBean {
	@ManagedProperty(value = "#{mongoUserAO}")
	private MongoUserAO mongoUserAO;
	
	
	@ManagedProperty(value = "#{userSession}")
	private UserSession userSession;

	private String username;
	private String password;

	public String login() {
		User loggedInUser = mongoUserAO.getUser(username, password);
		if(loggedInUser != null) {
			userSession.setUser(loggedInUser);
		}
		
		return "fileoverview";
	}

	public void validateLogin(FacesContext context, UIComponent component,
			Object value) {
		
		UIInput usernameComponent = (UIInput) component.getAttributes().get(
				"usernameForValidator");
		Object usernameFromValidatorValue = usernameComponent.getValue();
		String usernameFromValidator = 	usernameFromValidatorValue.toString();
				

		String passwordFromValidator = value.toString();

		User loggedInUser = mongoUserAO.getUser(usernameFromValidator,
				passwordFromValidator);

		if (loggedInUser == null) {
			
			ResourceBundle bundle = ResourceBundle.getBundle("language.error", context.getViewRoot().getLocale());
		    
			
			FacesMessage message = new FacesMessage(bundle.getString("login_failed"));
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(message);
		}
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public MongoUserAO getMongoUserAO() {
		return mongoUserAO;
	}

	public void setMongoUserAO(MongoUserAO mongoUserAO) {
		this.mongoUserAO = mongoUserAO;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public UserSession getUserSession() {
		return userSession;
	}

	public void setUserSession(UserSession userSession) {
		this.userSession = userSession;
	}

	
	

}
