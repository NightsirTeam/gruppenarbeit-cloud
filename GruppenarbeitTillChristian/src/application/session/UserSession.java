package application.session;

import javax.annotation.PreDestroy;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import application.datamodel.User;

/**
 * SessionBean for checking if user is logged in or not<br/>
 * (if username is set, user is logged in otherwise not)
 * 
 * @author Christian Sami
 *
 */
@ManagedBean
@SessionScoped
public class UserSession {
	private User user;

	public UserSession() {
		user = null;
	}
	
	public void setUser(User user) {
		this.user = user;
	}
	
	public String getUsername() {
		return user.getUsername();
	}
	
	public String getEmail() {
		return user.getEmail();
	}
	
	public String loggingOut() {
		user = null;
		return "index";
	}
	
	//if username is set user is logged in otherwise not
	public boolean isLoggedIn() {
		return user!=null;
	}
	
	@PreDestroy
	public void sessionDestroyed() {
		if(isLoggedIn()) {
			user = null;
		}
	}
}
