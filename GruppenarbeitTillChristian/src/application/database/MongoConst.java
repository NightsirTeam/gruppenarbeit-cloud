package application.database;

public class MongoConst {
	//Database
	public static final String HOST = "localhost";
	public static final int PORT = 27017;
	public static final String DATABASE = "gruppenarbeit";
	
	//Collections
	public static final String COLL_USER = "user";
	public static final String COLL_FILE = "file";
	
	//Buckets
	public static final String BUCKET_FILE = "filebucket";
	
	//Usercollection
	public static final String USER_USERNAME = "username";
	public static final String USER_PASSWORD = "password";
	public static final String USER_EMAIL = "email";
	
	//Filecollection
	public static final String FILE_FILENAME = "filename";
	public static final String FILE_USER = "user";
	public static final String FILE_FILE = "file";
	public static final String FILE_UPLOAD_DATE = "uploaddate";
	
	//Drive-Temp-Safe
	//Normaly C:/temp/ -> changes because off ssd
	public static final String TEMP_PATH = "C:/temp/";
}
