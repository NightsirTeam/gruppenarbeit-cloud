package application.database;

import java.net.UnknownHostException;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import application.datamodel.User;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.Mongo;

@ManagedBean
@ApplicationScoped
public class MongoUserAO {
	private Mongo mongo;
	
	public MongoUserAO() {
		try {
			mongo = new Mongo(MongoConst.HOST, MongoConst.PORT);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}
	
	public void addUser(String username, String password, String email) {
		DB db = mongo.getDB(MongoConst.DATABASE);
		DBCollection coll = db.getCollection(MongoConst.COLL_USER);
		
		BasicDBObject basicDBObject = new BasicDBObject(MongoConst.USER_USERNAME, username).
				append(MongoConst.USER_PASSWORD, password).
				append(MongoConst.USER_EMAIL, email);
		coll.insert(basicDBObject);
	}
	
	public User getUser(String username, String password) {
		DB db = mongo.getDB(MongoConst.DATABASE);
		DBCollection coll = db.getCollection(MongoConst.COLL_USER);
		
		BasicDBObject query = new BasicDBObject(MongoConst.USER_USERNAME, username).
				append(MongoConst.USER_PASSWORD, password);
		DBCursor cursor = coll.find(query);
		
		try {
			if(cursor.hasNext()) {
				DBObject dbObject = cursor.next();
				String user = (String)dbObject.get(MongoConst.USER_USERNAME);
				String email = (String)dbObject.get(MongoConst.USER_EMAIL);
				return new User(user, email);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public boolean isUserAlreadyExisting(String username) {
		DB db = mongo.getDB(MongoConst.DATABASE);
		DBCollection coll = db.getCollection(MongoConst.COLL_USER);
		
		BasicDBObject query = new BasicDBObject(MongoConst.USER_USERNAME, username);
		DBCursor cursor = coll.find(query);
		
		try {
			if(cursor.hasNext()) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return false;
	}
}
