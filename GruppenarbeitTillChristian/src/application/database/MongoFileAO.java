package application.database;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import application.filemanager.ShowFile;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.Mongo;
import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSInputFile;

@ManagedBean
@ApplicationScoped
public class MongoFileAO {
	private Mongo mongo;
	
	public MongoFileAO() {
		try {
			mongo = new Mongo(MongoConst.HOST, MongoConst.PORT);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}
	
	public List<ShowFile> getFileNamesFor(String username) {
		List<ShowFile> fileNames = new ArrayList<ShowFile>();
		DB db = mongo.getDB(MongoConst.DATABASE);
		DBCollection coll = db.getCollection(MongoConst.COLL_FILE);
		BasicDBObject query = new BasicDBObject(MongoConst.FILE_USER, username);
		DBCursor cursor = coll.find(query);
		try {
			while(cursor.hasNext()) {
				fileNames.add(new ShowFile((String)cursor.next().get(MongoConst.FILE_FILENAME)));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return fileNames;
	}
	
	public void writeFileToDB(InputStream inputStream, String name, String username) {
		try {
			String fileName = UUID.randomUUID().toString() + name + username;
			writeFileToDisk(fileName, inputStream);
			File file = new File(MongoConst.TEMP_PATH + fileName);
			
			DB db = mongo.getDB(MongoConst.DATABASE);
			GridFS gridFS = new GridFS(db, MongoConst.BUCKET_FILE);
			GridFSInputFile gridFSInputFile = gridFS.createFile(file);
			gridFSInputFile.save();
			
			file.delete();
			
			DBCollection coll = db.getCollection(MongoConst.COLL_FILE);
			BasicDBObject basicDBObject = new BasicDBObject(MongoConst.FILE_USER, username).
					append(MongoConst.FILE_FILENAME, name).
					append(MongoConst.FILE_FILE, fileName);
			coll.insert(basicDBObject);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void deleteFileInDB(String username, String filename) {
		DB db = mongo.getDB(MongoConst.DATABASE);
		DBCollection coll = db.getCollection(MongoConst.COLL_FILE);
		GridFS gridFS = new GridFS(db, MongoConst.BUCKET_FILE);
		
		BasicDBObject query = new BasicDBObject(MongoConst.FILE_USER, username).
				append(MongoConst.FILE_FILENAME, filename);
		DBCursor cursor = coll.find(query);
		try {
			if(cursor.hasNext()) {
				String file = (String)cursor.next().get(MongoConst.FILE_FILE);
				gridFS.remove(gridFS.findOne(file));
				coll.remove(query);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public InputStream readFileByUser(String username, String filename) {
		DB db = mongo.getDB(MongoConst.DATABASE);
		DBCollection coll = db.getCollection(MongoConst.COLL_FILE);
		BasicDBObject query = new BasicDBObject(MongoConst.FILE_USER, username).
				append(MongoConst.FILE_FILENAME, filename);
		DBCursor cursor = coll.find(query);
		try {
			if(cursor.hasNext()) {
				String file = (String)cursor.next().get(MongoConst.FILE_FILE);
				return readFileFromDB(file);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public InputStream readFileFromDB(String fileName) {
		DB db = mongo.getDB(MongoConst.DATABASE);
		GridFS gridFS = new GridFS(db, MongoConst.BUCKET_FILE);
		GridFSDBFile gridFSDBFile = gridFS.findOne(fileName);
		return gridFSDBFile.getInputStream();
	}
	
	private void writeFileToDisk(String fileName, InputStream in) {
		try {
			OutputStream out = new FileOutputStream(new File(MongoConst.TEMP_PATH + fileName));
			
			int read = 0;
			byte[] bytes = new byte[1024];
			
			while ((read = in.read(bytes)) != -1) {
				out.write(bytes, 0, read);
			}
			
			in.close();
			out.flush();
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}